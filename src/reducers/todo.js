import { CHANGE_QUERY, CHANGE_THEME, EDIT_ITEM, GET_ITEM, GET_ITEMS, REMOVE_ITEM, UPDATE_ITEM, USER_LOGIN } from '../actions/types';

const initialState = {
    theme: 'theme-light',
    user: null,
    query: '',
    item: null,
    items: [],
    total: 0,
}

const todo = (state = initialState, {type, ...data}) => {
    switch (type) {
        case GET_ITEM:
            return {
                ...state,
                item: data.item,
            };

        case GET_ITEMS:
            return {
                ...state,
                items: data.items,
                total: data.total,
            };

        case EDIT_ITEM:
            return {
                ...state,
                item: data.item
            };

        case UPDATE_ITEM:
            return {
                ...state,
                items: state.items.map((item) => item.id === data.item.id ? data.item : item),
            };

        case REMOVE_ITEM:
            return {
                ...state,
                items: state.items.filter((item) => item.id !== data.id),
            };

        case CHANGE_QUERY:
            return {
                ...state,
                query: data.query
            }

        case USER_LOGIN:
            return {
                ...state,
                user: data.user
            };

        case CHANGE_THEME:
            return {
                ...state,
                theme: state.theme === 'theme-light' ? 'theme-dark' : 'theme-light'
            }

        default:
            return state;
    }
}

export default todo;
