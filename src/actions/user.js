import * as Api from "../api";
import { USER_LOGIN } from "./types";

export const login = (username, password) => {
    return async dispatch => {
        const [user] = await Api.login(username, password);
        if (user) {
            dispatch({type: USER_LOGIN, user});
        }
    }
}
