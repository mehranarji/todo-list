import { CHANGE_THEME } from './types';

export const changeTheme = () => ({type: CHANGE_THEME});