export const GET_ITEM = "GET_ITEM";
export const GET_ITEMS = "GET_ITEMS";
export const REMOVE_ITEM = "REMOVE_ITEM";
export const TOGGLE_ITEM = "TOGGLE_ITEM";
export const INSERT_ITEM = "INSERT_ITEM";
export const UPDATE_ITEM = "UPDATE_ITEM";
export const EDIT_ITEM = "EDIT_ITEM";

export const CHANGE_QUERY = "CHANGE_QUERY";

export const USER_LOGIN = "USER_LOGIN";
export const USER_LOGOUT = "USER_LOGOUT";

export const CHANGE_THEME = "CHANGE_THEME";
