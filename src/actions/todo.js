import * as Api from "../api";
import { CHANGE_QUERY, EDIT_ITEM, GET_ITEM, GET_ITEMS, REMOVE_ITEM, UPDATE_ITEM } from './types';

export const getItem = (id) => {
    return async (dispatch, getState) => {
        const {user} = getState();
        const item = await Api.getItem(user, id);
        dispatch({type: GET_ITEM, item});
    }
}

export const getItems = (page) => {
    return async (dispatch, getState) => {
        const {user, query} = getState();
        const { items, total } = await Api.getItems(user, page, query);
        dispatch({ type: GET_ITEMS, items, total });
    }
}

export const insertItem = (title) => {
    return async (dispatch, getState) => {
        const {user} = getState();
        await Api.createItem(user, {
            title,
            done: false,
            date: new Date().toLocaleString()
        });
    }
}

export const editItem = (id, item) => {
    return {
        type: EDIT_ITEM,
        item: item
    }
}

export const updateItem = (id, item) => {
    return async (dispatch, getState) => {
        const {user} = getState();
        const newItem = await Api.updateItem(user, id, item);
        dispatch({
            type: UPDATE_ITEM,
            item: newItem
        });
    }
}

export const removeItem = (id) => {
    return async (dispatch, getState) => {
        const {user} = getState();
        await Api.removeItem(user, id);
        dispatch({
            type: REMOVE_ITEM,
            id: id
        })
    }
}

export const searchItems = (query) => {
    return async (dispatch, getState) => {
        dispatch({ type: CHANGE_QUERY, query });
    }
}
