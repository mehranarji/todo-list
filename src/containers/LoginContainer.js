import { connect } from "react-redux";
import { login } from '../actions/user';
import LoginForm from '../components/LoginForm';


const stateToProps = state => ({
    user: state.user,
})

const dispatchToProps = dispatch => ({
    onLogin: (username, password) => dispatch(login(username, password))
})

export default connect(stateToProps, dispatchToProps)(LoginForm);
