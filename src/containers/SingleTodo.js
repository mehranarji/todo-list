import { connect } from "react-redux";
import { editItem, getItem, removeItem, updateItem } from '../actions/todo';
import TodoSingle from '../components/TodoSingle';

const stateToProps = state => ({
    todo: state.item,
})

const dispatchToProps = dispatch => ({
    onUpdate: (id, item) => dispatch(updateItem(id, item)),
    onEdit: (id, item) => dispatch(editItem(id, item)),
    onRemove: id => dispatch(removeItem(id)),
    getItem: id => dispatch(getItem(id)),
})

export default connect(stateToProps, dispatchToProps)(TodoSingle);
