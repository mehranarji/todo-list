import { connect } from 'react-redux';
import ThemeToggle from '../components/ThemeToggle';
import { changeTheme } from '../actions/theme';

const stateToProps = state => ({
    theme: state.theme
})

const dispatchToProps = dispatch => ({
    onThemeChange: () => dispatch(changeTheme()),
});

export default connect(stateToProps, dispatchToProps)(ThemeToggle);
