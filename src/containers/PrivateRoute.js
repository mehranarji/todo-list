import React from "react";
import { connect } from 'react-redux';
import { Navigate } from 'react-router';

const PrivateRoute = ({user, children}) => {
    if (!user) {
        return <Navigate to="/" />
    }
    
    return children;
}

const stateToProps = state => ({
    user: state.user
});

export default connect(stateToProps, null)(PrivateRoute);