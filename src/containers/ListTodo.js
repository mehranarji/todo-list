import { connect } from "react-redux";
import { getItems, insertItem, removeItem, searchItems, updateItem } from '../actions/todo';
import TodoList from '../components/TodoList';

const stateToProps = state => ({
    items: state.items,
    total: state.total,
    query: state.query,
})

const dispatchToProps = dispatch => ({
    onInsert: (text, description) => dispatch(insertItem(text, description)),
    onUpdate: (id, item) => dispatch(updateItem(id, item)),
    onRemove: (id) => dispatch(removeItem(id)),
    getItems: page => dispatch(getItems(page)),
    onSearch: query => dispatch(searchItems(query))
})

export default connect(stateToProps, dispatchToProps)(TodoList);
