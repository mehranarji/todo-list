import React, { Suspense } from "react";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";

import PrivateRoute from "./containers/PrivateRoute";
import todo from "./reducers/todo";

import Loading from "./components/Loading";
import { Route, Routes } from "react-router-dom";

const Aside = React.lazy(() => import("./components/Aside"));
const LoginContainer = React.lazy(() => import('./containers/LoginContainer'));
const ListTodo = React.lazy(() => import("./containers/ListTodo"));
const SingleTodo = React.lazy(() => import("./containers/SingleTodo"));
const ThemeContainer = React.lazy(() => import("./containers/ThemeToggleContainer"));

const store = createStore(todo, applyMiddleware(thunk));

const App = () => {
  return (
    <Suspense fallback={<Loading />}>
      <Provider store={store}>
        <ThemeContainer>
        <div className="wrapper">
          <Aside />
          <main className="main">
            <Routes>
              <Route path="/" element={<LoginContainer />} />
              <Route path="/todo" element={<PrivateRoute><ListTodo /></PrivateRoute>} />
              <Route path="/todo/:todoId" element={<PrivateRoute><SingleTodo /></PrivateRoute>} />
            </Routes>
          </main>
        </div>
        </ThemeContainer>
      </Provider>
    </Suspense>
  );
};

export default App;
