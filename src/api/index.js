import axios from "axios";

export const baseUrl = `http://localhost:3001`
export const limit = 8;

const client = axios.create({
    baseURL: baseUrl
})

export const getItem = async (user, id) => {
    const { data } = await client.get(`/items/${id}`, {
        headers: {
            token: user.token
        }
    });
    return data;
};

export const getItems = async (user, page, query) => {
    const { data, headers } = await client.get(`/user/${user.id}/items`, {
        params: {
            title_like: query,
            _page: page,
            _limit: limit,
            _sort: 'date',
            _order: 'desc'
        },
        headers: {
            token: user.token
        }
    });
    return ({
        items: data,
        total: +headers['x-total-count']
    });
};

export const createItem = async (user, item) => {
    const { data } = await client.post(`/user/${user.id}/items`, item, {
        headers: {
            token: user.token
        }
    });
    return data;
}

export const updateItem = async (user, id, item) => {
    const { data } = await client.put(`/items/${id}`, { ...item }, {
        headers: {
            token: user.token
        }
    });
    return data;
}

export const removeItem = async (user, id) => {
    const { data } = await client.delete(`/items/${id}`, {
        headers: {
            token: user.token
        }
    });
    return data;
}

export const searchItems = async (user, query) => {
    const { data, headers } = await client.get(`/user/${user.id}/items`, {
        params: {
            _page: 1,
            _limit: limit,
            title_like: query,
            _sort: 'date',
            _order: 'desc'
        },
        headers: {
            token: user.token
        }
    });
    return ({
        items: data,
        total: +headers['x-total-count']
    });
}

export const login = async (username, password) => {
    const {data} = await client.get('/users', {
        params: { username, password }
    })
    return data;
}
