import React, { useMemo, useState } from "react";

const placeholders = [
  "Write it down, now",
  "Write it before you forget it",
  "If You Remember, No need to regret",
];

const TodoItemForm = ({ onSubmit, disabled }) => {
  const [text, setText] = useState("");
  const placeholder = useMemo(() => {
    if (disabled) {
      return "Please Wait...";
    }

    return placeholders[Math.floor(Math.random() * placeholders.length)]
  }, [text, disabled]);

  return (
    <div className="todo-form">
      <form
        onSubmit={(ev) => {
          ev.preventDefault();
          text.trim() && onSubmit(text.trim())
            .then(() => {
              setText("");
            });
        }}
      >
        <input
          type="text"
          className="todo-input"
          placeholder={placeholder}
          value={text}
          disabled={disabled}
          onChange={(ev) => setText(ev.target.value)}
        />
      </form>
    </div>
  );
};

export default TodoItemForm;
