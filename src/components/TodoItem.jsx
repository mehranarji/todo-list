import React from "react";
import className from "classnames";

const TodoItem = ({ todo, disabled, onEdit, onToggle, onRemove, onShow }) => {
  return (
    <fieldset className={className("todo-item", {'todo-item-done': todo.done, 'todo-item-disable': disabled})}>
      <div className="todo-state">
        <input type="checkbox" checked={todo.done} onChange={(ev) => onToggle && onToggle(ev.target.checked)} />
      </div>
      <p className="todo-title">{todo.title}</p>
      {onRemove && <button className="todo-remove" onClick={() => onRemove()}>&times;</button>}
      {onShow && <button className="todo-info" onClick={() => onShow()}>i</button>}
    </fieldset>
  );
};

export default TodoItem;
