import React from 'react';

const ThemeToggle = ({theme, onThemeChange, children}) => {
    return (
        <div className={theme}>
            <button className="theme-toggle" onClick={() => onThemeChange()}></button>
            {children}
        </div>
    );
}

export default ThemeToggle;
