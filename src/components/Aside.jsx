import React from "react";

const Aside = () => {
    return (
        <aside className="aside">
            <h1 className="app-title">A<br/>Simple<br/>Todo List</h1>
            <p className="app-lead">made with <span className="heart">♥</span> by <b>mehran arjmand</b></p>
        </aside>
    );
}

export default Aside;
