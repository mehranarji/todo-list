import React from "react";

const TodoSearch = ({onChange, value}) => {
    return (
        <input type="search" className="todo-search" value={value} onChange={ev => onChange(ev.target.value)} placeholder="Search"/>
    );
}

export default TodoSearch;
