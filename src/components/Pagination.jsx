import React from "react";

const Pagination = ({ page, total, onChange }) => {
  const siblingsCount = 2;

  return (
    <div className="pagination">
      <div className="pagination-list">
        <button
          className="pagination-item pagination-prev"
          disabled={page === 1}
          onClick={() => onChange && onChange(page - 1)}
        >
          &lsaquo;
        </button>
        {Array(total)
          .fill()
          .map((v, i) => {
            // Current Item
            if (i + 1 === page) {
              return (
                <button
                  key={i}
                  className="pagination-item pagination-active"
                  disabled
                >
                  {i + 1}
                </button>
              );
            }

            // Active buttons
            if (
              i + 1 === 1 ||
              i + 1 === total ||
              (i + 1 <= page + siblingsCount && i + 1 >= page - siblingsCount)
            ) {
              return (
                <button
                  key={i}
                  className="pagination-item"
                  onClick={() => onChange && onChange(i + 1)}
                >
                  {i + 1}
                </button>
              );
            }

            // More items
            if (
              i + 1 === page + siblingsCount + 1 ||
              i + 1 === page - siblingsCount - 1
            ) {
              return (
                <button key={i} className="pagination-item" disabled>
                  &hellip;
                </button>
              );
            }

            return null;
          })}

        <button
          className="pagination-item pagination-next"
          disabled={page === total}
          onClick={() => onChange && onChange(page + 1)}
        >
          &rsaquo;
        </button>
      </div>
    </div>
  );
};

export default Pagination;
