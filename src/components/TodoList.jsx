import React, { useEffect, useState } from "react";
import TodoItem from './TodoItem';
import TodoItemForm from './TodoItemForm';
import Pagination from './Pagination';
import TodoSearch from './TodoSearch';
import { limit } from '../api';
import { useNavigate } from 'react-router';

const TodoList = ({items, total, query, onInsert, getItems, onUpdate, onRemove, onSearch}) => {
    const [page, setPage] = useState(1);
    const [disabled, setDisabled] = useState(true);
    const totalPage = Math.ceil(total / limit);

    const navigate = useNavigate();

    useEffect(() => {
        getItems(page)
            .finally(() => setDisabled(false) )
    }, [page]);

    useEffect(() => {
        if (page === 1) {
            return getItems(page)
        }

        setPage(1);
    }, [query]);

    const onSubmit = (item) => {
        setDisabled(true);
        return onInsert(item)
            .then(() => getItems(page) )
            .finally(() => setDisabled(false) )
    }

    const toggleItem = (item) => {
        onUpdate(item.id, {...item, done: !item.done});
    }

    const removeItem = (item) => {
        onRemove(item.id);
    }

    const showItem = (item) => {
        navigate(`/todo/${item.id}`);
    }

    const onSearchChange = (query) => {
        onSearch(query.trim());
    }
    
    return (
        <div className="todo-container">
            <div className="todo-list">
                <TodoItemForm onSubmit={onSubmit} disabled={disabled}/>
                {onSearch && <TodoSearch value={query} onChange={(query) => onSearchChange(query)} />}
                {items?.map(item => 
                    <TodoItem
                        key={item.id}
                        todo={item}
                        onToggle={() => toggleItem(item)}
                        onRemove={() => removeItem(item)}
                        onShow={() => showItem(item)}
                        />
                )}
            </div>
            {totalPage > 1 && <Pagination page={page} total={totalPage} onChange={setPage} />}
        </div>
    )
}

export default TodoList;
