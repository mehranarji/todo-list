import React, { useEffect } from "react";
import { useParams } from "react-router";
import Loading from "./Loading";

const TodoSingle = ({ todo, getItem, onUpdate, onEdit }) => {
  const params = useParams();

  useEffect(() => {
    getItem(params.todoId);
  }, []);

  if (!todo) {
    return <Loading />;
  }

  const toggleItem = () => {
    onUpdate(todo.id, { ...todo, done: !todo.done });
  };

  const onChangeTitle = (title) => {
    onEdit(todo.id, {
      ...todo,
      title,
    });
  };

  const onChangeDescription = (description) => {
    onEdit(todo.id, {
      ...todo,
      description,
    });
  };

  const update = () => {
    if (!todo.title.trim()) {
      return;
    }

    onUpdate(todo.id, todo);
  };

  return (
    <div className="todo-container">
      <div className="todo-list">
        <fieldset className="todo-item">
          <div className="todo-state">
            <input
              type="checkbox"
              checked={todo.done}
              onChange={(ev) => toggleItem()}
            />
          </div>

          <input
            type="text"
            className="todo-title-input"
            value={todo.title}
            onChange={(ev) => onChangeTitle(ev.target.value)}
          />
          <textarea
            rows="5"
            className="todo-description-input"
            placeholder="Describe it..."
            value={todo.description || ""}
            onChange={(ev) => onChangeDescription(ev.target.value)}
          ></textarea>

          <time className="todo-date">{todo.date}</time>

          <button className="todo-edit" onClick={() => update()}>Edit</button>
        </fieldset>
      </div>
    </div>
  );
};

export default TodoSingle;
