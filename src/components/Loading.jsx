import React from "react";

const Loading = () => {
    return (
        <div className="loading">
            <p className="loading-text">Loading
                <span className="loading-circle"></span>
                <span className="loading-circle"></span>
                <span className="loading-circle"></span>
            </p>
        </div>
    );
}

export default Loading;
