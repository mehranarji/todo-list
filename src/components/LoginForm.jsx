import React, { useEffect, useState } from "react";
import { useNavigate } from 'react-router';

const LoginForm = ({user, onLogin}) => {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const navigate = useNavigate();

    useEffect(() => {
        user && navigate('/todo');
    }, [user])
    
    const onSubmit = () => {
        username && password && onLogin(username, password);
    };
    
    return (
        <div className="login-container">
            <form onSubmit={ev => ev.preventDefault() || onSubmit()}>
                <h2 className="login-title">Login Form</h2>
                <input type="text" className="login-input" placeholder="Username" value={username} onChange={ev => setUsername(ev.target.value)} />
                <input type="password" className="login-input" placeholder="Password" value={password} onChange={ev => setPassword(ev.target.value)} />
                <button type="submit" className="login-submit">Login</button>
            </form>
        </div>
    )
}

export default LoginForm;
